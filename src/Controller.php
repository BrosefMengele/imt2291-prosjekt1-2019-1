<?php

require 'twig/vendor/autoload.php';


class Controller{

    public function __construct(){
        $loader = new Twig_Loader_Filesystem('views/twig_templates');
        $twig = new Twig_Environment($loader);
        
        echo $twig->render('index.html', array('navn' =>'Ali Abdullahi Ali'));
    }
};