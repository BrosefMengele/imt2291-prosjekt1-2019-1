<?php

/**
 * Skal håndtere videoer administrerering til og fra db
 */

 class Playlist{
    private $db;

    /**
     * Contructoren kobler til db med PDO
     */
    public function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost; dbname=prosjekt1; charset=utf8','root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Error accured creating new PDO..";
            echo $e->getMessage();
        }
    }

    /**
     * Stenger kobling til db når objektet dør
     */
    public function __destruct() {
        if ($this->db!=null) {
          unset ($this->db);
        }
    }

    /**
     * Getting all the videos with that the logged student
     * @param string: id for the studenten
     * @return array: with the video the the student subscribed and status of the operation
     */
    public function getStudPlaylist($id){
        $tmp = [];

        try{
            $quary = 'select Favorit from students where id= ?';
            $smt = $this->db->prepare ($quary);
            $smt->execute(array($id));
            $emne = $smt->fetch(PDO::FETCH_ASSOC);
            $sql = 'Select * from videos where EmneCode = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array($emne['Favorit']));
            $tmp['vid'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $sql = 'Select rate, comments, videoId, studentId from videoinfo';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array());
            $tmp['videoInfo'] = $sth->fetchAll(PDO::FETCH_ASSOC); 
            $tmp['status'] = 'OK';
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to get the playlist';
            $tmp['errorInfo'] = $e->getMessage();
        }

        return $tmp;
    }

    /**
     * Getting all the videos with that the logged teacher
     * @param string: id for the lærer
     * @return array: with the video the the teacher subscribed and status of the operation
     */
    public function teacherPlaylist($id){
        $tmp = [];

        try{
            $quary = 'select Subject from teachers where id= ?';
            $smt = $this->db->prepare ($quary);
            $smt->execute(array($id));
            $emne = $smt->fetch(PDO::FETCH_ASSOC);
            $sql = 'Select * from videos where EmneCode = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array($emne['Subject']));
            $tmp['vid'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $sql = 'Select rate, comments from videoinfo';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array());
            $tmp['videoInfo'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $tmp['status'] = 'OK';
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to get the playlist';
            $tmp['errorInfo'] = $e->getMessage();
        }

        return $tmp;
    }

    /**
     *  Delating video from the playlist and DB
     * @param string: id for vid which will be delating
     * @return array: with info about the status of the operation
     */
    public function deleteVid($id){
        $tmp = [];

        try{
            $sql = 'delete from videos where id = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array($id));
            $tmp['status'] = 'OK';
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to get the playlist';
            $tmp['errorInfo'] = $e->getMessage();
        }

        return $tmp;
    }   

};