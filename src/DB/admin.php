<?php


/**
 * Class for admin users. Will execute admin actions to the DB
 * 
 */

 class Admin{
    private $db;

    /**
     * Contructor connects to the DB  with PDO
     */
    public function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost; dbname=prosjekt1; charset=utf8','root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Error accured creating new PDO..";
            echo $e->getMessage();
        }
    }

    /**
     * Disconnecting the DB when the objekt dies
     */
    public function __destruct() {
        if ($this->db!=null) {
          unset ($this->db);
        }
    }

    /**
     * Making new admin in DB.
     * @param: array with the data to the new admin
     * @return: array with feedback.
     */
    public function createNewAdmin($data){
        $tmp = [];
        try{
            $sql = 'insert into admin (first_name, last_name, email, password, created_by) values (?, ?, ?, ?, ?)';
            $sth = $this->db->prepare($sql);
            $sth->execute (array($data['fornavn'], $data['etternavn'], $data['email'], password_hash($data['passord'], PASSWORD_DEFAULT)
            ,$data['createdBy']));
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        if($sth->rowCount() == 1){
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into contact registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        return $tmp;
    }
	
	  /**
     *  Function which deletes admin
     *  @param integer: id to admin which will be deleting
     *  @return array: Status of deleting.
     */
    public function deleteAdmin($id){
        $tmp = [];

        try{
            $sql = 'DELETE FROM admin where id = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array($id));
            $tmp['status'] = 'OK';            
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }
	
	  /**
     * Approving new user(student or teacher)
     * @param: id and type of user.
     * @return: the result of the action(OK or FAIL)
     */
    public function approveUser($id, $user){
        $tmp = [];
        if($user == 'teacher'){
            try{
                $sql = 'UPDATE teachers set reg_godkjent = "JA" where id = ?';
                $sth = $this->db->prepare ($sql);
                $sth->execute (array($id));
                $tmp['status'] = 'OK';
                
            } catch (Exception $e) {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
    
        } else {
            try{
                $sql = 'UPDATE students set reg_godkjent = "JA" where id = ?';
                $sth = $this->db->prepare ($sql);
                $sth->execute (array($id));
                $tmp['status'] = 'OK';
                
            } catch (Exception $e) {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
        }
        
       
        return $tmp;
    }
 /**
     * For innlogging av admins.
     * @param: array med data (epost og passord)
     * @return: array med status-info osv for å sjekker om innloggingen var vellykket
     */
    public function logInAdmin($data){
        $tmp = [];

        try {
            $sql = 'SELECT id, password, first_name, last_name FROM admin WHERE email = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array ($data['email']));
            $data_array = $sth->fetch(PDO::FETCH_ASSOC);
            if(password_verify($data['password'], $data_array['password'])){
                $tmp['status'] = 'OK';
                $tmp['id'] = $data_array['id'];
                $tmp['fname'] = $data_array['first_name'];
                $tmp['lname'] = $data_array['last_name'];
                $tmp['person'] = "Admin";
            } else{
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Wrong email or password';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
            
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Wrong email or password';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        return $tmp;  
    }
 /**
     * Finner all userdata(Teachers og students data fra db)
     * @return: array med user- data.
     */
    public function getAllUserData(){
        $tmp = [];

        try{
            $sql = 'SELECT * FROM teachers';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array());
            
            $tmp['teachers'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $tmp['count']  = count($tmp['teachers']);
    
            $query= 'SELECT * FROM students';
            $sth= $this->db->prepare($query);
            $sth->execute(array());
    
            $tmp['students'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $tmp['count2'] = count($tmp['students']);
            $tmp['status'] = 'OK';
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
       
        return $tmp;

    }
	
 };