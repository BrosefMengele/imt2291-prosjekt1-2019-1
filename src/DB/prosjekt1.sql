-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2019 at 01:51 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prosjekt1`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `last_name`, `email`, `password`, `created_by`) VALUES
(1, 'admin', 'adminson', 'admin@admin.ntnu.com', '123', 0),

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `Fname` varchar(50) NOT NULL,
  `Lname` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `password` varchar(120) NOT NULL,
  `Favorit` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reg_godkjent` enum('JA','NEI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `Fname`, `Lname`, `Email`, `password`, `Favorit`, `date`, `reg_godkjent`) VALUES
(1, 'kafi', 'ali', 'kafi@stud.ntnu.com', '$2y$10$37GI7JBHdvqxpJtnt1Be6O.EBXg3o8l2FBPu6jQni6R2Bu8e6zfN6', 'IMT2243-Systemutvikling', '2019-02-12 15:14:30', 'NEI'),
(2, 'ayan', 'ali', 'ayan@stud.ntnu.com', '$2y$10$RX9R5nUd/oE.BVYAqyJ8n.ZBotYJ8e/FHaLgxSUWSmJJhqgn7Gmh.', 'IMT2006-Datanettverk', '2019-02-12 16:00:59', 'JA'),
(3, 'aisha', 'ali', 'a@stud.ntnu.com', '$2y$10$aHXyzJvLPFf5YMSSk6fO/upcHL9k0dmz5qPpRQbB46WvKFX9suUnu', 'IMT1031-Grunnleggende programmering', '2019-02-12 16:05:31', 'JA'),
(4, 'zahra', 'ali', 'zahra@stud.ntnu.com', '$2y$10$AZAu5o/Il1DYdraAygV2ZO8RLjRH6aZHoa3s0WkNIlEaeGbG/iM2.', 'IMT1031-Grunnleggende programmering', '2019-02-13 14:20:28', 'NEI'),
(5, 'ali', 'bariise', 'ali@stud.ntnu.com', '$2y$10$bOHwlZ054BLEerGhkolfDuSVlerJ84B1jvxBegNPpViXGTyCNpnIO', 'REA1101-Matematikk for informatikkfag', '2019-02-27 11:42:33', 'NEI');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `Fname` varchar(40) NOT NULL,
  `Lname` varchar(40) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Subject` varchar(50) NOT NULL,
  `Password` varchar(250) NOT NULL,
  `photo` blob NOT NULL,
  `reg_godkjent` enum('JA','NEI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `Fname`, `Lname`, `Email`, `Subject`, `Password`, `photo`, `reg_godkjent`) VALUES
(1, 'ali', 'bari', 'ali@sd.com', 'IMT2291-WWW-teknologi', '$2y$10$ecq0S.slwTTMKxb4UdXNBOSbbnvKaWja56bSEQw3iOL5Ihskm0nq2', '', 'NEI'),
(2, 'zahra', 'ahmed', 'zahra@zah.com', 'IMT1031-Grunnleggende programmering', '$2y$10$wpp7SFWpGSeGDyOggMZRyu/sjfboNzi8aq2.hrqEdk1YlqyspCVzW', '', 'JA'),
(3, 'yusra', 'ali', 'yus@yus.com', 'REA1101-Matematikk for informatikkfag', '$2y$10$WnAz6lOPBx.ZEBwCrM5/HOY32i/XGRnZGaLMhrhth1HZn1PXssW..', '', 'JA'),
(4, 'ali', 'ali', 'ali@ali.com', 'IMT1003-Innføring i IT-drift, InfoSec', '$2y$10$idk2sWAmo37VqE6SMjR6WeU.XlMc.yUHGPmMFHfhUcGmlL0rUleLu', '', 'JA'),
(5, 'ilkacas', 'ali', 'ilkacas@il.com', 'REA1101-Matematikk for informatikkfag', '$2y$10$pq64tAmXIbSrKCqaeJ6jF.590v7zQWqmDSka6jyphkzHiXo0XvYgm', '', 'JA'),
(6, 'baba', 'mama', 'ali@hot.com', 'IMT1031-Grunnleggende programmering', '$2y$10$2jav6sOjBp1jr/tFcMXx2uoaSUr6yOCkZGAXGBfv.eoxZxUJ4x/oy', '', 'NEI'),
(7, 'ali', 'ba', 'ali@som.com', 'IMT1003-Innføring i IT-drift, InfoSec', '$2y$10$S6zD2ZJtksVVj/lvNhSuTOM0jnVpt4BwYeRXOL49oqrjJeplR3gce', '', 'JA');

-- --------------------------------------------------------

--
-- Table structure for table `videoinfo`
--

CREATE TABLE `videoinfo` (
  `id` int(11) NOT NULL,
  `videoId` int(11) DEFAULT NULL,
  `rate` enum('1','2','3','4','5') DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `studentId` int(11) DEFAULT NULL,
  `teacherId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videoinfo`
--

INSERT INTO `videoinfo` (`id`, `videoId`, `rate`, `comments`, `studentId`, `teacherId`) VALUES
(1, 1, '3', NULL, 7, NULL),
(2, NULL, NULL, '           dårlig lærer    ', NULL, 7),
(3, NULL, NULL, 'Best lærer i verden.        ', NULL, 7),
(4, 1, '', NULL, 7, NULL),
(5, NULL, NULL, '     best lærere          ', NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `Cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Duration` int(11) NOT NULL,
  `Lecturer` varchar(80) NOT NULL,
  `EmneCode` varchar(50) NOT NULL,
  `Topic` varchar(50) NOT NULL,
  `Link` varchar(150) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `Cdate`, `Duration`, `Lecturer`, `EmneCode`, `Topic`, `Link`, `teacher_id`, `type`) VALUES
(1, '2019-03-01 10:40:06', 0, 'ali ba', 'IMT1003-Innføring i IT-drift, InfoSec', 'TestVid', '../DB/uploadVid.php/15c790c06e03e2developerStories-subtitles-en.vtt', 7, 'text'),
(2, '2019-03-01 12:01:43', 0, 'ali ba', 'IMT1003-Innføring i IT-drift, InfoSec', 'Beste vid', '../DB/uploadVid.php/15c791f279d50bdeveloperStories-subtitles-en.vtt', 7, 'text');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indexes for table `videoinfo`
--
ALTER TABLE `videoinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Topic` (`Topic`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `videoinfo`
--
ALTER TABLE `videoinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
