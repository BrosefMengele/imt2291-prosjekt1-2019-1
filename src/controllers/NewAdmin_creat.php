<?php


require_once '../twig/vendor/autoload.php';
require_once "../DB/admin.php";


$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['fname'])) {
  echo $twig->render('createAdmin.html', array());
} else {
  session_start();
  $data['fname'] = $_POST['fname'];
  $data['lname'] = $_POST['lname'];
  $data['email'] = $_POST['email'];
  $data['password'] = $_POST['password'];
  $data['createdBy'] = $_SESSION['adminId'];

  $newAdmin = new Admin();
  $res = $newAdmin->createNewAdmin ($data);
  $res['data'] = $data;
  if($res['status'] == 'OK'){
    echo "Created successfully new admin!";
    echo $twig->render('createAdmin.html', $res); 
  } else {
    echo "Couldnt  create admin! Try again..";
    echo $twig->render('newAdminCreated.html', array());
  }
  
}