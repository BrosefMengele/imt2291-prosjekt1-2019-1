<?php

require_once '../twig/vendor/autoload.php';
require_once '../DB/teacher_reg2.php';


$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['fname'])) {
  echo $twig->render('TeacherAddForm.html', array());
} else {
  $data['fname'] = $_POST['fname'];
  $data['lname'] = $_POST['lname'];
  $data['subject'] = $_POST['subject'];
  $data['email'] = $_POST['email'];
  $data['password'] = $_POST['password'];


  $teacher = new User();
  $res = $teacher->addTeacher ($data);
  $res['data'] = $data;

  echo $twig->render('addUser.html', $res); 
}