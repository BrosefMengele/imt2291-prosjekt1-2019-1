<?php

require_once '../twig/vendor/autoload.php';
require_once '../DB/uploadVid.php';
require_once '../DB/Allplaylist.php';

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();
if($_SESSION['logedIn']){
    if(isset($_GET['submit'])){
        $data['id'] = $_SESSION['id'];
        $data['rate'] = $_GET['rate'];
        $data['vidId'] = $_GET['id'];
        $video = new Video();
        $play = new Playlist();
        $res = $video->addRate($data);
        if($res['status'] == 'OK'){
            echo "Rating was added";
        } else{
            echo "Rating was NOT added";
            print_r($res);
        }
        $res['id'] = $_SESSION['id'];
        $res['fname'] = $_SESSION['fname'];
        $res['lname'] = $_SESSION['lname'];
        $res['person'] = $_SESSION['Teacher'];
        $res['videos'] = $play->getStudPlaylist($data['id']);
        echo $twig->render('playlistStudent.html', $res);
    }
};
