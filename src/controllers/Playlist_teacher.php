<?php

require_once '../twig/vendor/autoload.php';
require_once "../DB/uploadVid.php";
require_once "../DB/Allplaylist.php";

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();
if(isset($_SESSION['logedIn'])){
    $res['id'] = $_SESSION['id'];
    $res['fname'] = $_SESSION['fname'];
    $res['lname'] = $_SESSION['lname'];
    $res['person'] = $_SESSION['Teacher'];
    $playlist = new Playlist();
    $res['videos'] = $playlist->teacherPlaylist($res['id']);
    if($res['videos']['status'] == 'OK'){
        echo $twig->render('playlistTeacher.html', $res);
    } else {
        echo "Failed to get you're playlist";
        $res2['fname'] = $_SESSION['fname'];
        $res2['lname'] = $_SESSION['lname'];
        $res2['person'] = $_SESSION['Teacher'];
        $vid = new Video();
        $res2['videos'] =  $vid->getAllVideos();
        echo $twig->render('viewAllVideos.html', $res2);
    }
     
} else {
    echo $twig->render('login.html', array());
}
