<?php

require_once '../twig/vendor/autoload.php';
require_once "../DB/uploadVid.php";
require_once "../DB/Allplaylist.php";

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();


if($_SESSION['logedIn']){
    if(!isset($_POST['submit'])){
        echo $twig->render('postComment.html', array());
        $_SESSION['vidId'] = $_GET['id'];
    } else {
        $data['vidId'] = $_SESSION['vidId'];
        $data['id'] = $_SESSION['id'];
        $data['comments'] = $_POST['comments'];
        $data['person'] = $_SESSION['Teacher'];
        $video = new Video();
        $play = new Playlist();
        $res = $video->addComment($data);
        unset($_SESSION['vidId']);
        if($res['status'] == 'OK'){
            echo "comment was added";
        } else{
            echo "Comment was NOT added";
            print_r($res);
        }
        $res['id'] = $_SESSION['id'];
        $res['fname'] = $_SESSION['fname'];
        $res['lname'] = $_SESSION['lname'];
        $res['person'] = $_SESSION['Teacher'];
        if($res['person'] == 'student'){
            $res['videos'] = $play->getStudPlaylist($data['id']);
            echo $twig->render('playlistStudent.html', $res);
        } else {
            $res['videos'] = $play->teacherPlaylist($data['id']);
            echo $twig->render('playlistTeacher.html', $res);
        }
        
    }
}
