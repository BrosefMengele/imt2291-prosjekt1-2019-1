<?php

require_once '../twig/vendor/autoload.php';
require_once "../DB/teacher_reg2.php";
require_once '../DB/uploadVid.php';
require_once '../DB/admin.php';

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

    

if (!isset($_POST['email']) && !isset($_POST['password'])) {
  echo $twig->render('login.html', array());
} else {
  $res = [];
  $data['email'] = $_POST['email'];
  $data['password'] = $_POST['password'];

  $tmp = explode('@',$data['email']);
  $tmp = $tmp[1];
  $tmp = explode('.', $tmp);
  $userType = $tmp[0];
  $user = new User();
  if($userType == 'stud'){
    $res =$user->loginStudent($data);
    $res['data'] = $data;
  } elseif($userType == 'admin') {
    $admin = new Admin();
    $res = $admin->loginAdmin($data);
  } else {
    $res = $user->loginTeacher($data);
    $res['data'] = $data;
  }
  
  if($res['status'] == 'OK'){
    session_start();
    $vid = new Video();
    
    if($userType == 'admin'){
      $result = $admin->getAllUserData();
      $result['fname'] = $res['fname'];
      $result['lname'] = $res['lname'];
      $_SESSION['adminLoggedIn'] = true;
      $_SESSION['adminId'] = $res['id'];
      $_SESSION['fname'] = $res['fname'];
      $_SESSION['lname'] = $res['lname'];
      echo $twig->render('adminPage.html', $result);
    } elseif($userType == 'stud') {
      $_SESSION['logedIn'] = true;
      $_SESSION['id']= $res['id'];
      $_SESSION['Teacher'] = 'Student';
      $_SESSION['fname'] = $res['fname'];
      $_SESSION['lname'] = $res['lname'];
      echo "You are successfully logged in";
      $res['videos'] = $vid->getAllVideos();
      
      echo $twig->render('viewAllVideos.html', $res);
    } else {
      $_SESSION['id']= $res['id']; // You can set the value however you like.
      $_SESSION['logedIn'] = true;
      $_SESSION['fname'] = $res['fname'];
      $_SESSION['lname'] = $res['lname'];
      echo "You are successfully logged in";
      $res['videos'] = $vid->getAllVideos();
      $_SESSION['Teacher'] = $res['person']; 
      echo $twig->render('teacherNavigation.html', $res);
    }
     
  } else {
    echo '<h4>Wrong email or password!</h4>';
    echo $twig->render('login.html', array());
  }
   
}