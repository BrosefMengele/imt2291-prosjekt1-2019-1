<?php
require_once '../twig/vendor/autoload.php';
require_once "../DB/playlist.php";


$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();
if($_SESSION['logedIn']){
    $id = $_GET['id'];
    $play = new Playlist();
    $res = $play->deleteVid($id);
    if($res['status'] == 'OK'){
        echo "The video was delated";
    } else{
        echo "The video was NOT delated";
    }
    $res['id'] = $_SESSION['id'];
    $res['fname'] = $_SESSION['fname'];
    $res['lname'] = $_SESSION['lname'];
    $res['person'] = $_SESSION['Teacher'];
    $res['videos'] = $play->teacherPlaylist($res['id']);
    echo $twig->render('playlistTeacher.html', $res);
}

