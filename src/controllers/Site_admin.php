<?php


require_once '../twig/vendor/autoload.php';
require_once "Page_Admin.php";

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));



$page = new AdminPage();
$page->createPage();
