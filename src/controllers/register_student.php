
<?php

require_once '../twig/vendor/autoload.php';
require_once '../DB/teacher_reg2.php';

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['fname'])) {
  echo $twig->render('studentAdd.html', array());
} else {
  $data['fname'] = $_POST['fname'];
  $data['lname'] = $_POST['lname'];
  $data['fav'] = $_POST['fav'];
  $data['email'] = $_POST['email'];
  $data['password'] = $_POST['password1'];

  
  $student = new User();
  $res = $student->addStudent ($data);
  $res['data'] = $data;

  echo $twig->render('addUser.html', $res);
  
}
