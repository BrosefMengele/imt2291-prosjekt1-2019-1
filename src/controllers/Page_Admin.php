<?php


require_once '../twig/vendor/autoload.php';
require_once "../DB/admin.php";

/**
 *  For rendering a adminpage. Creates a adminpage.
 */
class AdminPage{

    private $page;
    private $twig;

    /**
     *  Constructor that initiates twig and starts a session.
     */
    public function __construct(){
        $loader = new Twig_Loader_Filesystem('./../View/twig_templates');
        $this->twig = new Twig_Environment($loader, array(
            //'cache' => './compilation_cache',
        ));
        
        session_start();
    }

    /**
     * The function that renderes a adminpage with all userdata.
     * @param: nothing
     * @return: nothing
     */

    public function createPage(){
        if(isset($_SESSION['adminLoggedIn'])){
            if($_SESSION['adminLoggedIn'] == true){
                $admin = new Admin();
                $this->page =  $admin->getAllUserData();
                $this->page['adminId'] = $_SESSION['adminId'];
                $this->page['fname'] = $_SESSION['fname'];
                $this->page['lname'] = $_SESSION['lname'];
                //$res1['admin'] = $res;
                //print_r($res);
                echo $this->twig->render('adminPage.html', $this->page);
            } else {
                echo $this->twig->render('login.html', array());
            }
            
        } else {
            echo $this->twig->render('login.html', array());
        }
        
    }

};