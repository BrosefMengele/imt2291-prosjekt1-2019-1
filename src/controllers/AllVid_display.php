<?php

require_once '../twig/vendor/autoload.php';
require_once "../DB/uploadVid.php";

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();
if(isset($_SESSION['logedIn'])){
    $res['id'] = $_SESSION['id'];
    $res['fname'] = $_SESSION['fname'];
    $res['lname'] = $_SESSION['lname'];
    $res['person'] = $_SESSION['Teacher'];
    $vid = new Video();
    $res['videos'] =  $vid->getAllVideos();
    
    echo $twig->render('viewAllVideos.html', $res);
} else {
    echo $twig->render('login.html', array());
}
