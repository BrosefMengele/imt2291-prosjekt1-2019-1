<?php


require_once '../twig/vendor/autoload.php';
require_once '../DB/uploadVid.php';

$loader = new Twig_Loader_Filesystem('./../views/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));


if (!isset($_POST['submit'])) {
  echo $twig->render('uploadVideo.html', array());
} else {
  //$data['id'] = $_GET['id'];
  $data['tmp_n']= $_FILES['file']['name'];
  $data['tmp_dest'] = $_FILES['file']['tmp_name'];
  $data['error'] = $_FILES['file']['error'];
  $data['size'] = $_FILES['file']['size'];
  $type = $_FILES['file']['type'];
  $data['topic'] = $_POST['topic'];

  $temp = explode('/', $type);
  $data['type'] = $temp[0];
  
  
  if ($data['error'] === 0) {
     
    session_start();
    $data['id'] = $_SESSION['id'];
    $data['person'] = $_SESSION['Teacher'];
    $data['filename'] = uniqid(''. true).$data['tmp_n'];
    $data['destination'] = '../DB/uploadVid.php/'.$data['filename'];
    move_uploaded_file($data['tmp_dest'], $data['destination']);
    $vid = new Video();
    $res['id'] = $data['id'];
    $res['videos'] = $vid->getAllVideos(); 

    if ($data['size'] != 0) { 
      //$display = new displayVids($res);
      $res = $vid->addVideo ($data);
      echo "video was added";
    }
    echo $twig->render('viewAllVideos.html', $res);

  } else {
    echo "Something went wrong!";
    echo $twig->render('uploadVideo.html', array());
  }
   
}