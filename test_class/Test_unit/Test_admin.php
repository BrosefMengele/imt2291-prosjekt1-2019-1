<?php

require_once '../../src/DB/teacher_reg2.php';
require_once '../../src/DB/admin.php';

/**
 * class for admin tests.
 */

 class adminTest extends PHPUnit_Framework_TestCase{
    private $user;
    private $admin;
    private $student;
    private $teacher;
    private $test_admin;

     /**
      *  Starts before evry tests in the class, initiating data to
      *  used to in the tests
      */
     protected function setup(){
        try {
            $this->user = new User;
            $this->admin = new Admin;

            //Student who will be used in the tests
            $this->student['lname'] = md5(date('l jS \of F Y h:i:s B'));  // Create random 32 character string
            $this->student['fname'] = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
            $this->student['fav'] = "imt2218";
            $this->student['email'] = $this->student['fname']."@".$this->student['lname']."no";
            $this->student['password'] = "123";

            //The teacher who will be used in the tests
            $this->teacher['lname'] = md5(date('l jS \of F Y h:i:s B'));  
            $this->teacher['fname'] = md5(date('l jS \of F Y h:i:s A'));  
            $this->teacher['subject'] = "imt2218";
            $this->teacher['email'] = $this->teacher['fname']."@".$this->teacher['lname']."no";
            $this->teacher['password'] = "123";

            //The teacher who will be used in the tests
            $this->test_admin['lname'] = md5(date('l jS \of F Y h:i:s B'));  
            $this->test_admin['fname'] = md5(date('l jS \of F Y h:i:s A'));  
            $this->test_admin['createdBy'] = "3";
            $this->test_admin['email'] = $this->test_admin['fname']."@".$this->test_admin['lname']."no";
            $this->test_admin['password'] = "123";
            
        } catch (Exception $e) {
            return "Fail". $e->getMessage();
        }
    }
    /**
     * This method is called after the last test of this test class is run.
     */
    protected function tearDown(){
        unset($this->user);
        unset($this->admin);
        unset($this->teacher);
        unset($this->student);
        unset($this->test_admin);
    }

    /**
     *  Test for creating and deleting a admin
     */
    public function testIfANewAdminCanBeCreated(){
        $res = $this->admin->createNewAdmin($this->test_admin);
        $this->assertTrue('OK' == $res['status'], 'Couldt create a new admin');
        $id = $res['id'];
        //deleting the admin who was created
        $this->assertTrue($id > 0, "Id should not be bigger that 0");
        $info = $this->admin->deleteAdmin($id);
        $this->assertSame('OK', $info['status'], 'Couldt delete the admin');
    }

    /**
     *  Test that a admin can login.
     */
    public function testIfAnAdminCanLogIn(){
        //Creating a new admin to test if he can login
        $res = $this->admin->createNewAdmin($this->test_admin);
        $this->assertTrue('OK' == $res['status'], 'Couldt create a new admin');
        $id = $res['id'];

        $loginInfo['email'] = $this->test_admin['email'];
        $loginInfo['password'] = $this->test_admin['password'];
    
        $res2 = $this->admin->logInAdmin($loginInfo);
        $this->assertTrue($res2['status'] == 'OK', 'Admin couldt login');

        //Testing that wrong email or password cant login
        $wrongInfo['email'] = "test@test.com";
        $wrongInfo['password'] = "123456";
        $wrongInfo2['email'] = $this->test_admin['email'];
        $wrongInfo2['password'] = "123456";
        $wrongInfo3['email'] = "test@test.com";;
        $wrongInfo3['password'] = $this->test_admin['password'];

        $res3 = $this->admin->logInAdmin($wrongInfo);
        $res4 = $this->admin->logInAdmin($wrongInfo2);
        $res5 = $this->admin->logInAdmin($wrongInfo3);
        $this->assertTrue($res3['status'] == 'FAIL', 'Admin loggedin with wrong info');
        $this->assertTrue($res4['status'] == 'FAIL', 'Admin loggedin with wrong info');
        $this->assertTrue($res4['status'] == 'FAIL', 'Admin loggedin with wrong info');

        //deleting the admin who was created
        $this->assertTrue($id > 0, "Id should not be bigger that 0");
        $info = $this->admin->deleteAdmin($id);
        $this->assertSame('OK', $info['status'], 'Couldt delete the admin');
    }

    /**
     *  Testing that the admin can get all the data from db
     *  Testing the function getAllUserData() in Admin- class
     */
    public function testThatAdminCanGetAllUserDataFromDb(){
        $res = $this->admin->createNewAdmin($this->test_admin);
        $this->assertTrue('OK' == $res['status'], 'Couldt create a new admin');
        $id = $res['id'];

        $data = $this->admin->getAllUserData();
        $this->assertNotEmpty($data['teachers']);
        $this->assertNotEmpty($data['students']);
        $this->assertTrue('OK' == $data['status'], 'admin cant get the data');
        $this->assertTrue($data['count'] > 0 && $data['count2'] > 0, 'No student or teachers found');

        //deleting the test- admin who was created
        $this->assertTrue($id > 0, "Id should not be bigger that 0");
        $info = $this->admin->deleteAdmin($id);
        $this->assertSame('OK', $info['status'], 'Couldt delete the admin');
    }

    /**
     *  Test that the admin can delete students and teachers
     *  Test the function deleteUser in the Admin- class
     */

    public function testThatAdminCanDeleteUserFromDb(){
        $res = $this->user->addStudent($this->student);
        $this->assertTrue('OK' == $res['status'], 'Couldt create a new student');
        $id = $res['id'];

        $data = $this->admin->deleteUser($id, 'student');
        $this->assertSame($data['status'], 'OK', 'Couldt delete student');

        $res2 = $this->user->addTeacher($this->teacher);
        $this->assertTrue('OK' == $res2['status'], 'Couldt create a new teacher');
        $id2 = $res2['id'];

        $data2 = $this->admin->deleteUser($id2, 'teacher');
        $this->assertSame($data2['status'], 'OK', 'Couldt delete teacher');
    }

    /**
     *  Testing if a admin can approve new user (student/ teacher) so they can login
     *  Testing also that new users cant login before getting approved by an admin
     *  Testing function approveUser() in Admin- class
     */
    public function testIfAnAdminCanApproveAnUser(){
        $res = $this->admin->createNewAdmin($this->test_admin);
        $this->assertTrue('OK' == $res['status'], 'Couldt create a new admin');
        $id = $res['id'];

        // Creating a new student for testing
        $res = $this->user->addStudent($this->student);
        $this->assertTrue('OK' == $res['status'], 'Couldt create a new student');
        $id2 = $res['id'];
        $studLoginInfo['email'] = $this->student['email'];
        $studLoginInfo['password'] = $this->student['password'];

        // Creating a new teacher for testing
        $res2 = $this->user->addTeacher($this->teacher);
        $this->assertTrue('OK' == $res2['status'], 'Couldt create a new teacher');
        $id3 = $res2['id'];
        $teacherLoginInfo['email'] = $this->teacher['email'];
        $teacherLoginInfo['password'] = $this->teacher['password'];

        // Testing that new user cant login before getting approved
        $info = $this->user->loginStudent($studLoginInfo);
        $this->assertFalse('Ok' == $info['status'], "Student logged in without approval");
        $this->assertSame('FAIL', $info['status'], "Student logged in without approval");

        // Testing that new teacher cant login with approval
        $info2 = $this->user->loginTeacher($teacherLoginInfo);
        $this->assertFalse('Ok' == $info2['status'], "Teacher logged in without approval");
        $this->assertSame('FAIL', $info2['status'], "Teacher logged in without approval");

        // Testing that admin can approve the users above so they can loginn
        $res3 = $this->admin->approveUser($id2, 'student');
        $res4 = $this->admin->approveUser($id3, 'teacher');
        $this->assertSame('OK', $res3['status'], 'Approving student failed');
        $this->assertSame('OK', $res4['status'], 'Approving teacher failed');
         
        $info3 = $this->user->loginStudent($studLoginInfo);
        $this->assertTrue('OK' == $info3['status'], 'loggin failed with approval');
        
        // deleting the test admin, stundet and teacher who was created
        $this->assertTrue($id > 0, "Id should not be bigger that 0");
        $info = $this->admin->deleteAdmin($id);
        $this->assertSame('OK', $info['status'], 'Couldt delete the admin');

        $data = $this->admin->deleteUser($id2, 'student');
        $this->assertSame($data['status'], 'OK', 'Couldt delete student');
        $data2 = $this->admin->deleteUser($id3, 'teacher');
        $this->assertSame($data2['status'], 'OK', 'Couldt delete teacher');
    }

 }