<?php
//use PHPUnit\Framework\TestCase;

require_once '../vendor/autoload.php';
require_once '../../src/DB/teacher_reg2.php';
require_once '../../src/DB/admin.php';


/**
 * Testing the functionality of user registration / login / deletion etc
 * Testing the functions in User and Admin- class 
 */
class UserTest extends PHPUnit_Framework_TestCase {

    private $user;
    private $admin;
    private $student;
    private $teacher;

    /**
     *  Creating one instance of user and admin, and data for one student and teacher
     *  That can be used in the tests.
     */
    protected function setup(){
        try {
            $this->user = new User;
            $this->admin = new Admin;

            //Student who will be used in the tests
            $this->student['lname'] = md5(date('l jS \of F Y h:i:s B'));  // Create random 32 character string
            $this->student['fname'] = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
            $this->student['fav'] = "imt2218";
            $this->student['email'] = $this->student['fname']."@".$this->student['lname']."no";
            $this->student['password'] = "123";

            //The teacher who will be used in the tests
            $this->teacher['lname'] = md5(date('l jS \of F Y h:i:s B'));  // Create random 32 character string
            $this->teacher['fname'] = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
            $this->teacher['subject'] = "imt2218";
            $this->teacher['email'] = $this->teacher['fname']."@".$this->teacher['lname']."no";
            $this->teacher['password'] = "123";
        
            return "OK";
        } catch (Exception $e) {
            return "Fail". $e->getMessage();
        }
    }
    /**
     * This method is called after the last test of this test class is run.
     */
    protected function tearDown(){
        unset($this->user);
        unset($this->admin);
        unset($this->teacher);
        unset($this->student);
    }

    /* public function testTrueAssertsToTrue(){
        //$this->assertTrue(true);
    }

    public function testCanConnectToDB() {
        
    } */

    /**
     * Testist if a student can be registered and deleted
     */
    public function testIfStudentCanBeRegisteredAndDeleted(){
        
        $res = $this->user->addStudent($this->student);
        $this->assertSame('OK', $res['status'], "Failed to add student");
        $personID = $res['id'];
        $this->assertTrue($personID > 0, "person ID should be > 0");
        
        //Deleting the student that was created
        $myUser = "student";
        $info = $this->admin->deleteUser($personID, $myUser);
        $this->assertSame('OK', $info['status'], 'The student could not be deleted');
    }

    /**
     * Testing if a teacher can be registered/deleted(by the admin of cource)
     * Testing the two functions in User->addTeacher and admin->deleteUser
     * 
     */
    public function testIfTeacherCanBeRegisteredAndDeleted(){
        
        $res = $this->user->addTeacher($this->teacher);
        $this->assertSame('OK', $res['status'], "Failed to add teacher");
        $personID = $res['id'];
        $this->assertTrue($personID > 0, "person ID should be > 0");
        
        //Deleting the student that was created
        $myUser = "teacher";
        $info = $this->admin->deleteUser($personID, $myUser);
        $this->assertSame('OK', $info['status'], 'The teacher could not be deleted');
    }

    /**
     *  Testing that a teacher who just registered cant login before an
     *  admin can approve him
     */
     public function testTeacherCantLoginBeforeGetApprovedByAdmin(){
    
        $res = $this->user->addTeacher($this->teacher);
        $this->assertSame('OK', $res['status'], "Failed to add teacher");
        $personID = $res['id'];
        $this->assertTrue($personID > 0, "person ID should be > 0");

        //testing that the login fails
        $loginInfo['email'] = $this->teacher['email'];
        $loginInfo['password'] = $this->teacher['password'];
        $feedback = $this->user->loginTeacher($loginInfo);
        $this->assertSame('FAIL', $feedback['status'], 'Teacher logged in without approval!');

        //Deleting the teacher who was created for the test from db
        $myUser = "teacher";
        $info = $this->admin->deleteUser($personID, $myUser);
        $this->assertSame('OK', $info['status'], 'The teacher could not be deleted');
     }

     /**
      *  Testing a student can't login when he registeres
      *  without getting approved by an admin first.
      */
      public function testThatStudentCantLoginBeforeGettingApproved(){

        $res = $this->user->addStudent($this->student);
        $this->assertSame('OK', $res['status'], "Failed to add student");
        $personID = $res['id'];
        $this->assertTrue($personID > 0, "person ID should be > 0");

          //testing that the login fails
        $loginInfo['email'] = $this->student['email'];
        $loginInfo['password'] = $this->student['password'];
        $feedback = $this->user->loginStudent($loginInfo);
        $this->assertSame('FAIL', $feedback['status'], 'student logged in without approval!');

        //Deleting the student who was created for the test from db
        $myUser = "student";
        $info = $this->admin->deleteUser($personID, $myUser);
        $this->assertSame('OK', $info['status'], 'The student could not be deleted');
      }

}